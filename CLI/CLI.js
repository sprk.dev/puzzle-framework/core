"use strict";

const cli = require("@puzzleframework/cli");

const I18nCLIMiddleware = require("../i18n/I18nCLIMiddleware");
const puzzle = require("../Puzzle");
const PRuntime = require("../Core/PRuntime");

/**
 * Class CLI
 */
class CLI extends PRuntime {
  static Loader = cli.Loader;

  static run = cli.run;

  static cli = cli;

  /**
   *
   * @protected
   *
   * @property {CMiddleware[]}
   */
  _middlewares = [];

  use(engine) {
    engine.set("cli", this);

    this.pushMiddleware(new I18nCLIMiddleware());
  }

  /**
   *
   * @param {...CMiddleware} middlewares
   */
  pushMiddleware(...middlewares) {
    this._middlewares.push(...middlewares);
  }

  boot() {
    super.boot();

    this._middlewares.forEach((middleware) => {
      middleware.use(puzzle);
    });
  }
}

module.exports = CLI;
