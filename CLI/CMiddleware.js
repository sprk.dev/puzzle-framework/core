"use strict";

const PObject = require("../Core/PObject");

/**
 *
 * @abstract
 */
class CMiddleware extends PObject {
  /**
   *
   * @abstract
   *
   * @param {PEngine} engine
   */
  use(engine) {

  }
}

module.exports = CMiddleware;
