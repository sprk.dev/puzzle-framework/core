"use strict";

const PRuntime = require("../Core/PRuntime");

/**
 * Class PConsoleRuntime
 *
 * @abstract
 */
class PConsoleRuntime extends PRuntime {
  /**
   * Should this runtime be ran only in CLI environment?
   *
   * @protected
   *
   * @property {boolean}
   */
  _cliOnly = true;
}

module.exports = PConsoleRuntime;
