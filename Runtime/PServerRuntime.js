"use strict";

const PRuntime = require("../Core/PRuntime");

/**
 * Class PServerRuntime
 *
 * @abstract
 */
class PServerRuntime extends PRuntime {
  /**
   * Should this runtime be ran only in HTTP environment?
   *
   * @protected
   *
   * @property {boolean}
   */
  _httpOnly = true;
}

module.exports = PServerRuntime;
