# Changelog

## 2.0 - New world release

**Codename:** New world release

**New features**
- [NEW] Migration to Typescript

**Replaced code**
- [REPLACE] puzzle.routes has been replaced by puzzle.controllers

**Removed code**
- [REMOVED] PEnum has been replaced by the usage of the `enum` keyword
- [REMOVED] index.import has been removed. Now you can import modules as needed, since there will not be framework inheritance

**Bug fixes**
- [BUG] Register the HTTP module correctly
- [BUG] Allow the registration of various modules, runtimes or state machines in the application and access the easily.
- [BUG] Export puzzle from `server.ts` file
- [BUG] Register the HTTP module
- [BUG] Configuration missing from the engine/puzzle instance
- [BUG] Missing loader for internationalisation
- [BUG] Missing loader for controllers
- [BUG] Module and Controller loading for situations when `export default` is used
- [BUG] Some minor bugfixes in the engine

**Enhancements**
- [ENHANCE] Async modules, load them correctly
- [ENHANCE] ConfigurationNotFoundException for the cases when there is no configuration available, when needed.
- [ENHANCE] Logging the routes that are registered by a controller
- [ENHANCE] Set the full path before registering the routes
- [ENHANCE] Abstract routers registering method
- [ENHANCE] Modules property in IApplication has been set to optional


**Still needs migration**
- Features from the old @puzleframework/core project

