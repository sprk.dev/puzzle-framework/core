"use strict";

const PEngine = require("../Core/PEngine");

/**
 * Class Puzzle
 */
class Puzzle extends PEngine {
  boot() {
    this.log.debug("App Boot");
  }
}

module.exports = Puzzle;
