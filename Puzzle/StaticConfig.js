"use strict";

const consign = require("consign");

const PConfig = require("../Core/PConfig");
const Defaults = require("../Defaults");

/**
 * Static configuration
 *
 * @module Puzzle
 */
class StaticConfig extends PConfig {
  constructor() {
    super();
    this.clear();
    this._config = { ...Defaults };
    this.reloadProperties();
  }

  /**
   * Initializes the configuration.
   */
  init() {
    const configFromFile = {};
    consign({
      cwd: process.cwd(),
      verbose: false,
      extensions: [".js", ".json", ".node", ".ts"],
      loggingType: "info"
    })
      .include(`./config`)
      .into(configFromFile);

    this.load(configFromFile.config);
  }

  /**
   * Initializes the configuration module on engine.
   *
   * @param {PEngine} engine The reference to engine class
   */
  use(engine) {
    engine.set("config", this);
    this.init();
  }
}

module.exports = StaticConfig;
