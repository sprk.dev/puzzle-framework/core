"use strict";

/**
 * Module instance definition.
 *
 * @module Puzzle
 *
 * @typedef {object} IModuleInstance
 * @property {string} name The name of the module.
 * @property {PRuntime} instance The instance of the module.
 */
