"use strict";

const Puzzle = require("./Puzzle");
const StaticConfig = require("./StaticConfig");
const I18n = require("../i18n/I18n");
const ModuleLoader = require("./ModuleLoader");

/**
 *
 * @module Puzzle
 */

const puzzle = new Puzzle();

puzzle.use(new StaticConfig());
puzzle.use(new I18n());
puzzle.use(new ModuleLoader(puzzle));

module.exports = puzzle;
