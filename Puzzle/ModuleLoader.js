"use strict";

const path = require("path");

const PState = require("../Core/PState");
const PRuntime = require("../Core/PRuntime");

const InvalidInstanceTypeException = require("../Exceptions/InvalidInstanceTypeException");

let puzzle = null;

/**
 * Class ModuleLoader
 */
class ModuleLoader extends PState {
  /**
   * The modules object - stores all the modules loaded into the application.
   *
   * @protected
   *
   * @property {Map<string, PRuntime>}
   */
  _modules = {};

  /**
   * The modules array - stores all the modules loaded into the application.
   *
   * @protected
   *
   * @property {IModuleInstance[]}
   */
  _orderedLoad = [];

  constructor(engine) {
    super();

    puzzle = engine;
  }

  /**
   * Registers a module with a given name and instance. It registers new instances only
   * when the application is in preboot state.
   *
   * @param {string} moduleName The name of the module.
   * @param {PRuntime} moduleInstance The instance of the module.
   *
   * @throws InvalidInstanceType
   */
  register(moduleName, moduleInstance) {
    if (this.state === "") {
      if (!this._canBeRunInContext(moduleInstance)) {
        return;
      }
      if (!(moduleInstance instanceof PRuntime)) {
        throw new InvalidInstanceTypeException("PRuntime");
      }

      if (!this.isValid(this._modules[moduleName])) {
        this._orderedLoad.push({
          name: moduleName,
          instance: moduleInstance
        });
        this._modules[moduleName] = this._orderedLoad.length - 1;
      } else {
        this._orderedLoad[this._modules[moduleName]].instance = moduleInstance;
      }

      moduleInstance.use(puzzle);
    }
  }

  /**
   * Load the modules into the system.
   *
   * @protected
   *
   * @param {string} moduleName The name of the module we want to load.
   */
  async _loadModule(moduleName) {
    const instancePaths = this.getModulePath(moduleName);
    let instance = null;
    puzzle.log.debug("Loading module: [%s].", moduleName);
    let error = "";

    for (const instancePath of instancePaths) {
      try {
        instance = require(instancePath);
        instance = instance.default || instance;
        instance = instance.getInstance ? instance.getInstance() : new instance();

        this.register(moduleName, instance);
        return;
      } catch (e) {
        error = e.message;
        instance = null;
      }
    }

    if (!this.isValid(instance)) {
      puzzle.log.error("Unable to load module [%s].", moduleName);
      puzzle.log.error("Unable to find the module in the following paths: [%s].",
        instancePaths.join("; "));
      puzzle.log.debug(error);
    }
  }

  /**
   * Load the modules defined in the package.json file of the application.
   */
  async loadFromPackage() {
    for await (const module of puzzle.app.modules) {
      await this._loadModule(module);
    }
  }

  /**
   * Attaches the module loader to the engine.
   *
   * @param {PEngine} engine The reference to engine class
   */
  use(engine) {
    engine.set("modules", this);
  }

  /**
   * Returns a list with paths where to look for a module.
   *
   * @protected
   *
   * @param {string} module Module name.
   *
   * @return {string[]}
   */
  getModulePath(module) {
    const pathList = [];

    pathList.push(module);
    pathList.push(path.join(process.cwd(), "puzzles", module));
    pathList.push(path.join(process.cwd(), "puzzles", module.replace(new RegExp(/\./, "gi"), "/")));
    pathList.push(path.join(process.cwd(), "src", "puzzles", module));
    pathList.push(path.join(process.cwd(), "src", "puzzles", module.replace(new RegExp(/\./, "gi"), "/")));
    pathList.push(path.join(process.cwd(), "dist", "puzzles", module));
    pathList.push(path.join(process.cwd(), "dist", "puzzles", module.replace(new RegExp(/\./, "gi"), "/")));

    return pathList;
  }

  /**
   * Can the given module be ran in current context (HTTP or CLI).
   *
   * @protected
   *
   * @param {PRuntime} module The module we are currently running.
   *
   * @return {boolean}
   */
  _canBeRunInContext(module) {
    if (puzzle.http && module.cliOnly === true) {
      return false;
    }
    return !(puzzle.cli && module.httpOnly === true);
  }

  /**
   * Runs the given stage for all the loaded modules.
   *
   * @protected
   *
   * @param {string} stage The stage we are currently running.
   */
  async _runStage(stage) {
    puzzle.log.debug(`Run stage: ${stage}`);
    let order = this._orderedLoad.slice();

    order = stage.toLowerCase()
      .indexOf("shutdown") >= 0 ? order.reverse() : order;

    for await (const module of order) {
      puzzle.log.debug(`Run stage: [${stage}] for module: [${module.name}]`);
      await module.instance[stage]();
    }

    puzzle.log.debug(`Finalized stage: ${stage}`);
  }

  /**
   * The code that executes before the Boot status is achieved.
   */
  async beforeBoot() {
    await super.beforeBoot();
    await this._runStage("beforeBoot");
  }

  /**
   * The code that executes when the Boot status is achieved.
   */
  async boot() {
    await this.beforeBoot();
    await super.boot();
    await this._runStage("boot");
    await this.afterBoot();
  }

  /**
   * The code that executes after the Boot status is achieved.
   */
  async afterBoot() {
    await super.afterBoot();
    await this._runStage("afterBoot");
  }

  /**
   * The code that executes before the Server Online status is achieved.
   */
  async beforeOnline() {
    await super.beforeOnline();
    await this._runStage("beforeOnline");
  }

  /**
   * The code that executes when the Server Online status is achieved.
   */
  async online() {
    await this.beforeOnline();
    await super.online();
    await this._runStage("online");
    await this.afterOnline();
  }

  /**
   * The code that executes after the Server Online status is achieved.
   */
  async afterOnline() {
    await super.afterOnline();
    await this._runStage("afterOnline");
  }

  /**
   * The code that executes before the Shutdown status is achieved.
   */
  async beforeShutdown() {
    await super.beforeShutdown();
    await this._runStage("beforeShutdown");
  }

  /**
   * The code that executes when the Shutdown status is achieved.
   */
  async shutdown() {
    await this.beforeShutdown();
    await super.shutdown();
    await this._runStage("shutdown");
    await this.afterShutdown();
  }

  /**
   * The code that executes after the Shutdown status is achieved.
   */
  async afterShutdown() {
    await super.afterShutdown();
    await this._runStage("afterShutdown");
  }
}

module.exports = ModuleLoader;
