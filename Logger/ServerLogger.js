"use strict";

const fs = require("fs");
const path = require("path");
const { rotator: rotate } = require("logrotator");
const Logger = require("log");

const LogLevel = require("./LogLevel");
const PLog = require("../Core/PLog");
const puzzle = require("../Puzzle");

class ServerLogger extends PLog {
  /**
   * File Stream used for logger.
   *
   * @private
   *
   * @property {WriteStream}
   */
  #stream = null;

  /**
   * Constructor of the Server Logger class.
   */
  constructor() {
    super();
    this.logLevel = puzzle.config.engine.debug ? "debug" : puzzle.config.engine.log.level;
    this.initLog();
  }

  /**
   * Creates the logger object that is going to be used by the application.
   *
   * @param {string} [logLevel=null] The level of logging.
   */
  initLog(logLevel = "") {
    const { log } = puzzle.config.engine;
    const logConfig = log;

    if (this.isValid(logLevel) && logLevel !== "" && typeof logLevel === "string") {
      this.logLevel = logLevel;
    }
    if (LogLevel[this.logLevel] < 0) {
      this.logLevel = LogLevel.INFO;
    }

    if (!this.isValid(logConfig.file) || logConfig.file === "") {
      this.logger = new Logger(this.logLevel);
      return;
    }

    if (logConfig.rotate === true) {
      rotate.register(logConfig.file, {
        schedule: logConfig.schedule || "5m",
        count: logConfig.count,
        size: logConfig.size,
        compress: logConfig.compress
      });
    }

    this.#stream = fs.createWriteStream(path.resolve(logConfig.file), {
      flags: "a"
    });
    this.logger = new Logger(
      this.logLevel,
      this.#stream
    );
  }
}

module.exports = ServerLogger;
