"use strict";

const fs = require("fs");
const path = require("path");
const Logger = require("log");

const PLog = require("../Core/PLog");
const puzzle = require("../Puzzle");

class RunnerLogger extends PLog {
  /**
   * File Stream used for logger.
   *
   * @private
   *
   * @property {WriteStream}
   */
  #stream = null;

  /**
   * Constructor of the Server Logger class.
   */
  constructor(fileName = "") {
    super();
    this.logLevel = puzzle.config.engine.runner.level;
    this.initLog(fileName);
  }

  /**
   * Creates the logger object that is going to be used by the application.
   *
   * @param {string} [fileName=""] The logging file.
   */
  initLog(fileName = "") {
    if (!this.isValid(fileName) || fileName === "") {
      this.logger = new Logger(this.logLevel);
      return;
    }

    this.#stream = fs.createWriteStream(path.resolve(fileName), {
      flags: "a"
    });
    this.logger = new Logger(
      this.logLevel,
      this.#stream
    );
  }
}

module.exports = RunnerLogger;
