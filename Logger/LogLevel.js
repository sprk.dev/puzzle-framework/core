"use strict";

/**
 * Logging level
 *
 * @module Logger
 *
 * @typedef {string} LogLevel
 */

module.exports = {
  ERROR: "error",
  WARNING: "warning",
  NOTICE: "notice",
  INFO: "info",
  DEBUG: "debug"
};
