"use strict";

const PObject = require("../Core/PObject");

/**
 * Class DummyLogger
 */
class DummyLogger extends PObject {
  debug(...params) {
  }

  info(...params) {
  }

  error(...params) {
  }

  verbose(...params) {
  }

  trace(...params) {
  }

  notice(...params) {
  }

  warning(...params) {
  }
}

module.exports = DummyLogger;
