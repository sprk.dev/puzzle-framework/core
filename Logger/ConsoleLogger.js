"use strict";

const PLog = require("../Core/PLog");
const DummyLogger = require("./DummyLogger");

/**
 * Class ConsoleLogger
 */
class ConsoleLogger extends PLog {
  /**
   * Constructor of the Console Logger class.
   */
  constructor() {
    super();
    this.logLevel = "";
    this.initLog();
  }

  /**
   * Creates the logger object that is going to be used by the application.
   *
   * @param {string} [logLevel=''] The level of logging.
   */
  initLog(logLevel = "") {
    this.logger = new DummyLogger();
  }
}

module.exports = ConsoleLogger;
