"use strict";

const path = require("path");
const moment = require("moment");

const PObject = require("../Core/PObject");
const puzzle = require("../Puzzle");
const RunnerLogger = require("../Logger/RunnerLogger");

/**
 * Class PRunner
 *
 * @abstract
 */
class PRunner extends PObject {
  /**
   * Screen logger reference.
   *
   * @protected
   *
   * @property {PLog|null}
   */
  _logger = null;

  /**
   * Runner file logger reference.
   *
   * @protected
   *
   * @property {PLog|null}
   */
  _fileLogger = null;

  /**
   * Should the file logging be disabled?
   *
   * @protected
   *
   * @property {boolean}
   */
  _disableFileLogging = false;

  /**
   * Runner constructor.
   *
   * @protected
   *
   * @param {Object|null} logger Logging class.
   */
  constructor(logger = null) {
    super();

    this._logger = logger || puzzle.log;
    this._fileLogger = new RunnerLogger(this.logFile);
    this._disableFileLogging = false;
    this._success = true;
  }

  /**
   * Was the runner executed successfully?
   *
   * @protected
   *
   * @property {boolean}
   */
  _success = false;

  /**
   * Was the runner executed successfully.
   *
   * @return {boolean}
   */
  get success() {
    return this._success;
  }

  /**
   * Returns the runner log file.
   *
   * @return {string}
   */
  get logFile() {
    const { runner } = puzzle.config.engine;

    const logsLocation = path.resolve(runner.logs);
    const timestamp = moment().format(runner.timestamp);
    return `${logsLocation}/runner.${this.className}.${timestamp}.log`;
  }

  /**
   * Returns the runner logger class.
   *
   * @protected
   *
   * @return {IRunnerLogger}
   */
  get log() {
    return {
      /**
       * Emergency log message.
       *
       * @param {...*} [args] The messages to be logged.
       */
      emergency: (...args) => {
        this._log("emergency", ...args);
      },
      /**
       * Alert log message.
       *
       * @param {...*} [args] The messages to be logged.
       */
      alert: (...args) => {
        this._log("alert", ...args);
      },
      /**
       * Critical log message.
       *
       * @param {...*} [args] The messages to be logged.
       */
      critical: (...args) => {
        this._log("critical", ...args);
      },
      /**
       * Error log message.
       *
       * @param {...*} [args] The messages to be logged.
       */
      error: (...args) => {
        this._log("error", ...args);
      },
      /**
       * Warning log message.
       *
       * @param {...*} [args] The messages to be logged.
       */
      warning: (...args) => {
        this._log("warning", ...args);
      },
      /**
       * Notice log message.
       *
       * @param {...*} [args] The messages to be logged.
       */
      notice: (...args) => {
        this._log("notice", ...args);
      },
      /**
       * Info log message.
       *
       * @param {...*} [args] The messages to be logged.
       */
      info: (...args) => {
        this._log("info", ...args);
      },
      /**
       * Debug log message.
       *
       * @param {...*} [args] The messages to be logged.
       */
      debug: (...args) => {
        this._log("debug", ...args);
      },
    };
  }

  /**
   * Enable the file logging system.
   */
  enableFileLogging() {
    this._disableFileLogging = false;
  }

  /**
   * Disable the file logging system.
   */
  disableFileLogging() {
    this._disableFileLogging = true;
  }

  /**
   * Logs a message in the logger.
   *
   * @protected
   *
   * @param {string} type The type of message.
   * @param {...*} [args] The messages to be logged.
   */
  _log(type, ...args) {
    type = type.toLowerCase();
    if (!this._disableFileLogging) {
      this._fileLogger.logger[type](...args);
    }

    if (this.isValid(this._logger)) {
      this._logger[type](...args);
    }
  }

  /**
   * Code executed by the runner.
   *
   * @async
   */
  async run(...args) {
    this._success = true;
  }
}

module.exports = PRunner;
