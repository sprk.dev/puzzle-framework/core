"use strict";

/**
 * Application definition.
 *
 * @module TaskRunner
 *
 * @typedef {object} IRunnerLogger
 * @method {void} emergency The emergency logger.
 * @param {...*} args The logging arguments.
 * @method {void} alert The alert logger.
 * @param {...*} args The logging arguments.
 * @method {void} critical The critical logger.
 * @param {...*} args The logging arguments.
 * @method {void} error The error logger.
 * @param {...*} args The logging arguments.
 * @method {void} warning The warning logger.
 * @param {...*} args The logging arguments.
 * @method {void} notice The notice logger.
 * @param {...*} args The logging arguments.
 * @method {void} info The info logger.
 * @param {...*} args The logging arguments.
 * @method {void} debug The debug logger.
 * @param {...*} args The logging arguments.
 */
