"use strict";

const { json, urlencoded } = require("body-parser");

const PMiddleware = require("../PMiddleware");

/**
 * Class BodyParser
 */
class BodyParser extends PMiddleware {
  use(engine, http) {
    http.use(json());
    http.use(urlencoded({
      extended: true
    }));
  }
}

module.exports = BodyParser;
