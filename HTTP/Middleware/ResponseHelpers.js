"use strict";

const PMiddleware = require("../PMiddleware");
const HTTPStatus = require("../HTTPStatus");

/**
 * Class ResponseHelpers
 */
class ResponseHelpers extends PMiddleware {
  use(engine, http) {
    http.use((req, res, next) => {
      /**
       * Returns an OK message to the API.
       *
       * @param {string} message The message to be sent.
       */
      res.ok = function ok(message) {
        res.json({
          status: "ok",
          message: engine.i18n.__(message)
        });
      };

      /**
       * Returns an error to the API.
       *
       * @param {string} type The type of the message.
       * @param {string} message The message to be sent.
       * @param {HTTPStatus} code The HTTP status code.
       */
      res.error = function error(type, message, code) {
        res.status(code || HTTPStatus.SERVER_ERROR).json({
          status: "error",
          statusCode: code || HTTPStatus.SERVER_ERROR,
          type,
          message: engine.i18n.__(message)
        });
      };

      /**
       * Throws an error to the API.
       *
       * @param {Object} error The error thrown by the application.
       */
      res.throw = function throws(error) {
        const { name: errorType } = error.constructor;
        const { message: errorMessage } = error;
        const code = error.httpStatus || HTTPStatus.SERVER_ERROR;

        engine.log.error(errorMessage);
        engine.log.error(error.stack);

        res.error(errorType, errorMessage, code);
      };
      next();
    });
  }
}

module.exports = ResponseHelpers;
