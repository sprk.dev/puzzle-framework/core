"use strict";

const cors = require("cors");

const PMiddleware = require("../PMiddleware");

/**
 * Class CORS
 */
class CORS extends PMiddleware {
  use(engine, http) {
    const { config } = engine;

    http.use(cors({
      credentials: true,
      origin: config.http.cors
    }));
  }
}

module.exports = CORS;
