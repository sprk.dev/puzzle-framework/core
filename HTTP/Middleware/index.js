"use strict";

const BodyParser = require("./BodyParser");
const CookieParser = require("./CookieParser");
const CORS = require("./CORS");
const I18nHTTPMiddleware = require("../../i18n/I18nHTTPMiddleware");
const ResponseHelpers = require("./ResponseHelpers");
const Session = require("./Session");

/**
 * The HTTP module of the application.
 *
 * @module HTTP.Middleware
 */


/**
 * Module index
 *
 * TODO: Update this documentation
 */
module.exports = [
  new BodyParser(),
  new CookieParser(),
  new CORS(),
  new I18nHTTPMiddleware(),
  new ResponseHelpers(),
  new Session()
];
