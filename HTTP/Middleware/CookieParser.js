"use strict";

const cookieParser = require("cookie-parser");

const PMiddleware = require("../PMiddleware");

/**
 * Class CookieParser
 */
class CookieParser extends PMiddleware {
  use(engine, http) {
    http.use(cookieParser());
  }
}

module.exports = CookieParser;
