"use strict";

const cookieParser = require("cookie-parser");
const session = require("express-session");

const PMiddleware = require("../PMiddleware");

/**
 * Class Session
 */
class Session extends PMiddleware {
  use(engine, http) {
    const { config } = engine;

    switch (config.session.store) {
      case "memory":
        http.use(session({
          cookieParser: cookieParser(),
          key: config.session.key,
          secret: config.session.secret,
          resave: false,
          saveUninitialized: true
        }));
        break;
    }
  }
}

module.exports = Session;
