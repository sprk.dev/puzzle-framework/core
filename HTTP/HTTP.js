"use strict";

const express = require("express");
const http = require("http");

const baseMiddlewares = require("./Middleware");
const HTTPStatus = require("./HTTPStatus");
const puzzle = require("../Puzzle");
const PRuntime = require("../Core/PRuntime");

/**
 * Class HTTP
 */
class HTTP extends PRuntime {
  /**
   *
   * @property {express.Application}
   */
  static app = null;

  /**
   *
   * @property {http.Server}
   */
  static http = null;

  /**
   *
   * @protected
   *
   * @property {PMiddleware[]}
   */
  _middlewares = [];

  constructor() {
    super();

    HTTP.app = express();
    HTTP.http = new http.Server(HTTP.app);
  }

  use(engine) {
    engine.set("http", this);

    this.pushMiddleware(...baseMiddlewares);
  }

  /**
   *
   * @param {...PMiddleware} middlewares A list of middlewares to be used in the application.
   */
  pushMiddleware(...middlewares) {
    this._middlewares.push(...middlewares);
  }

  beforeBoot() {
    super.beforeBoot();

    this._middlewares.forEach((middleware) => {
      middleware.use(puzzle, HTTP.app);
    });
  }

  online() {
    super.online();

    const version = puzzle.appVersion || puzzle.version;
    HTTP.http.listen(puzzle.config.http.port, puzzle.config.http.listen);
    puzzle.log.info("%s [v%s]", puzzle.config.engine.name || "Spark Puzzle Framework Lite", version.version);
    puzzle.log.info("-".repeat(30));
    puzzle.log.info("Listening on: %s:%d", puzzle.config.http.listen, puzzle.config.http.port);
    puzzle.log.info("-".repeat(30));
    HTTP.app.use(this._errorHandler);

    puzzle.log.info("HTTP Module is UP and Running!");
  }

  /**
   *
   * @protected
   */
  _errorHandler(err, req, res, next) {
    puzzle.log.error(req.path);
    puzzle.log.error(err.stack);

    let statusCode = err.httpStatus || HTTPStatus.SERVER_ERROR;
    if (err.name === "ValidationError") {
      statusCode = HTTPStatus.BAD_REQUEST;
    }

    const error = {
      status: "error",
      statusCode,
      errorCode: err.code,
      type: err.name,
      message: err.message
    };

    res.status(statusCode).json(error);
  }
}

module.exports = HTTP;
