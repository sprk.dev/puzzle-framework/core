"use strict";

/**
 * HTTP status
 *
 * @module HTTP
 *
 * @typedef {number} HTTPStatus
 */

module.exports = {
  BAD_REQUEST: 400,
  UNAUTHORIZED: 401,
  FORBIDDEN: 403,
  NOT_FOUND: 404,
  SERVER_ERROR: 500,
};
