"use strict";

const PObject = require("../Core/PObject");

/**
 *
 * @abstract
 */
class PMiddleware extends PObject {
  /**
   *
   * @abstract
   *
   * @param {PEngine} engine
   * @param {express.Application} http
   */
  use(engine, http) {

  }
}

module.exports = PMiddleware;
