"use strict";

const Controller = require("./Controller");
const puzzle = require("../../Puzzle");

/**
 * Class APIController
 *
 * @abstract
 */
class APIController extends Controller {
  /**
   * Stores the base of the API Path.
   *
   * @protected
   *
   * @property {string}
   */
  APIPath = "/api/master";

  /**
   * A list with middlewares that apply to some routes exposed by this class.
   *
   * @protected
   *
   * @property {object} middlewares
   * @property {Function[]} middlewares.group
   * @property {Function[]} middlewares.before
   * @property {Function[]} middlewares.after
   * @property {Function[]} middlewares.api
   */
  middlewares = {
    group: [],
    before: [],
    after: [],
    api: []
  };

  /**
   * Creates an instance of an API route.
   *
   * @protected
   *
   * @param {string} path The path to the API route.
   */
  constructor(path) {
    super(path);

    this.APIPath = puzzle.config.http.APIPath || "/api/master";
  }

  /**
   * Attaches the route path to the api path.
   */
  build() {
    this.path = `${this.APIPath}/${this.path}`.replace(/[/]+/g, "/");

    super.build();
  }
}

module.exports = APIController;
