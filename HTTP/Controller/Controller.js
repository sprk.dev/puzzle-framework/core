"use strict";

const { Router } = require("express");

const PObject = require("../../Core/PObject");
const { URLBuilder } = require("../Utils");
const HTTP = require("../HTTP");
const puzzle = require("../../Puzzle");

/**
 * Class Controller
 *
 * @abstract
 */
class Controller extends PObject {
  /**
   * The URL router of the current route.
   *
   * @protected
   *
   * @property {Router}
   */
  router = null;

  /**
   * The path served by this route.
   *
   * @protected
   *
   * @property {string}
   */
  path = "";

  /**
   * Should this class be imported?
   *
   * @protected
   *
   * @property {boolean}
   */
  noImport = false;

  /**
   * A list with middlewares that apply to some routes exposed by this class.
   *
   * @protected
   *
   * @property {object} middlewares
   * @property {Function[]} middlewares.group
   * @property {Function[]} middlewares.before
   * @property {Function[]} middlewares.after
   */
  middlewares = {
    group: [],
    before: [],
    after: []
  };


  /**
   * Creates an instance of the route.
   *
   * @protected
   *
   * @param {string} path The path to the route.
   */
  constructor(path) {
    super();

    this.router = Router({
      mergeParams: true
    });

    this.path = path || "";
  }

  /**
   * Builds the route in the express engine.
   */
  build() {
    this.path = URLBuilder(this.path);
    this.register();

    HTTP.app.use(
      this.path,
      this.middlewares.group || [],
      this.router
    );
  }

  /**
   * Register all the routes exposed by this route.
   *
   * @protected
   *
   * @abstract
   */
  register() {

  }

  /**
   * Appends middlewares before/after a route is run.
   *
   * @protected
   *
   * @param {string} when When the action happens: before or after the middleware/route ran.
   * @param {Function[]} middlewares A list with all the middlewares.
   *
   * @return {Function[]}
   */
  buildMiddlewares(when, middlewares) {
    return [...middlewares || [], ...this.middlewares[when] || []];
  }

  /**
   * Registers a route with a given type and a callback method.
   *
   * @protected
   *
   * @param {string} type The type of request (GET, POST, PUT, DELETE).
   * @param {string} path The path of the route.
   * @param {Function} callback The callback method.
   */
  routeRegister(type, path, callback) {
    this.routeRegisterMiddleware(type, path, null, callback);
  }

  /**
   * Registers a route with a given type, middleware list name and a callback method.
   *
   * @protected
   *
   * @param {string} type The type of request (GET, POST, PUT, DELETE).
   * @param {string} path The path of the route.
   * @param {string|null} middleware The name of the middleware list.
   * @param {Function} callback The callback method.
   */
  routeRegisterMiddleware(type, path, middleware, callback) {
    if (!this.isValid(middleware) || middleware === "before" || middleware === "after") {
      middleware = "";
    }
    puzzle.log.debug(`[${this.className}] Registering route: ${type.toUpperCase()} ${this.path}${path}`);
    this.router[type](
      path,
      ...this.middlewares[middleware] || [],
      ...this.middlewares.before || [],
      async (req, res, next) => {
        try {
          await callback.call(this, req, res, next);
        } catch (e) {
          if (this.isValid(e.details)) {
            let messages = "";
            e.details.forEach((message) => {
              messages += `${message.message}\n`;
            });
            res.error(e.name, messages.trim());
            return;
          }

          res.throw(e);
        }
      },
      ...this.middlewares.after || []
    );
  }

  /**
   * Registers a route with a given type, middleware list name and a callback method.
   *
   * @protected
   *
   * @param {string} type The type of request (GET, POST, PUT, DELETE).
   * @param {string} path The path of the route.
   * @param {...Function} middlewares The name of the middleware list.
   */
  routeRegisterMiddlewareList(type, path, ...middlewares) {
    puzzle.log.debug(`[${this.className}] Registering route: ${type.toUpperCase()} ${this.path}${path}`);
    const callback = middlewares.pop();
    this.router[type](path, ...middlewares, async (req, res, next) => {
      try {
        await callback.call(this, req, res, next);
      } catch (e) {
        if (this.isValid(e.details)) {
          let messages = "";
          e.details.forEach((message) => {
            messages += `${message.message}\n`;
          });
          res.error(e.name, messages.trim());
          return;
        }

        res.throw(e);
      }
    });
  }

  /* -- HTTP NO ADDITIONAL MIDDLEWARES VERBS -- */
  /**
   * Registers a GET route.
   *
   * @protected
   *
   * @param {string} path The path of the route.
   * @param {...Function} middlewares The callback method | middlewares.
   */
  get(path, ...middlewares) {
    if (middlewares.length === 1) {
      this.routeRegister("get", path, middlewares.pop());
      return;
    }

    this.routeRegisterMiddlewareList("get", path, ...middlewares);
  }

  /**
   * Registers a POST route.
   *
   * @protected
   *
   * @param {string} path The path of the route.
   * @param {...Function} middlewares The callback method | middlewares.
   */
  post(path, ...middlewares) {
    if (middlewares.length === 1) {
      this.routeRegister("post", path, middlewares.pop());
      return;
    }

    this.routeRegisterMiddlewareList("post", path, ...middlewares);
  }

  /**
   * Registers a PUT route.
   *
   * @protected
   *
   * @param {string} path The path of the route.
   * @param {...Function} middlewares The callback method | middlewares.
   */
  put(path, ...middlewares) {
    if (middlewares.length === 1) {
      this.routeRegister("put", path, middlewares.pop());
      return;
    }

    this.routeRegisterMiddlewareList("put", path, ...middlewares);
  }

  /**
   * Registers a PATCH route.
   *
   * @protected
   *
   * @param {string} path The path of the route.
   * @param {...Function} middlewares The callback method | middlewares.
   */
  patch(path, ...middlewares) {
    if (middlewares.length === 1) {
      this.routeRegister("patch", path, middlewares.pop());
      return;
    }

    this.routeRegisterMiddlewareList("patch", path, ...middlewares);
  }

  /**
   * Registers a DELETE route.
   *
   * @protected
   *
   * @param {string} path The path of the route.
   * @param {...Function} middlewares The callback method | middlewares.
   */
  delete(path, ...middlewares) {
    if (middlewares.length === 1) {
      this.routeRegister("delete", path, middlewares.pop());
      return;
    }

    this.routeRegisterMiddlewareList("delete", path, ...middlewares);
  }

  /* -- HTTP MIDDLEWARE GROUPS VERBS -- */
  /**
   * Registers a GET route with a middleware.
   *
   * @protected
   *
   * @param {string} path The path of the route.
   * @param {string} middlewareGroup The middleware list used for various things.
   * @param {Function} callback The callback method.
   */
  getMiddleware(path, middlewareGroup, callback) {
    this.routeRegisterMiddleware("get", path, middlewareGroup, callback);
  }

  /**
   * Registers a POST route with a middleware.
   *
   * @protected
   *
   * @param {string} path The path of the route.
   * @param {string} middlewareGroup The middleware list used for various things.
   * @param {Function} callback The callback method.
   */
  postMiddleware(path, middlewareGroup, callback) {
    this.routeRegisterMiddleware("post", path, middlewareGroup, callback);
  }

  /**
   * Registers a PUT route with a middleware.
   *
   * @protected
   *
   * @param {string} path The path of the route.
   * @param {string} middlewareGroup The middleware list used for various things.
   * @param {Function} callback The callback method.
   */
  putMiddleware(path, middlewareGroup, callback) {
    this.routeRegisterMiddleware("put", path, middlewareGroup, callback);
  }

  /**
   * Registers a PATCH route with a middleware.
   *
   * @protected
   *
   * @param {string} path The path of the route.
   * @param {string} middlewareGroup The middleware list used for various things.
   * @param {Function} callback The callback method.
   */
  patchMiddleware(path, middlewareGroup, callback) {
    this.routeRegisterMiddleware("patch", path, middlewareGroup, callback);
  }

  /**
   * Registers a DELETE route with a middleware.
   *
   * @protected
   *
   * @param {string} path The path of the route.
   * @param {string} middlewareGroup The middleware list used for various things.
   * @param {Function} callback The callback method.
   */
  deleteMiddleware(path, middlewareGroup, callback) {
    this.routeRegisterMiddleware("delete", path, middlewareGroup, callback);
  }

  /* -- MIDDLEWARE MANAGEMENT -- */
  /**
   * Pushes a middleware to the group middleware stack.
   *
   * @protected
   *
   * @param {function} middleware The middleware to be pushed to the stack.
   */
  pushGroup(middleware) {
    this.pushMiddleware("group", middleware);
  }

  /**
   * Pushes a middleware to the before middleware stack.
   *
   * @protected
   *
   * @param {function} middleware The middleware to be pushed to the stack.
   */
  pushBefore(middleware) {
    this.pushMiddleware("before", middleware);
  }

  /**
   * Pushes a middleware to the after middleware stack.
   *
   * @protected
   *
   * @param {function} middleware The middleware to be pushed to the stack.
   */
  pushAfter(middleware) {
    this.pushMiddleware("after", middleware);
  }

  /**
   * Pushes a middleware to the given middleware stack.
   *
   * @protected
   *
   * @param {string} middlewareName The name of the middleware stack.
   * @param {function} middleware The middleware to be pushed onto the stack.
   */
  pushMiddleware(middlewareName, middleware) {
    if (!this.isValid(this.middlewares[middlewareName])) {
      this.middlewares[middlewareName] = [];
    }

    this.middlewares[middlewareName].push(middleware);
  }
}

module.exports = Controller;
