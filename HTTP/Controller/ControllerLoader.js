"use strict";

const path = require("path");
const fs = require("fs");

const PUse = require("../../Core/PUse");
const puzzle = require("../../Puzzle");

/**
 * Class ControllerLoader
 */
class ControllerLoader extends PUse {
  /**
   * @protected
   *
   * @property {string[]}
   */
  _supportedExt = [".js", ".mjs"];

  use(engine) {
    engine.set("controllers", this);
  }

  /**
   * Register a routes folder into the engine.
   *
   * @param {string} root The root folder of the module.
   * @param {string} [routes=routes] The name of the routes folder.
   */
  async register(root, routes = "routes") {
    const fullPath = path.join(root, routes);
    const files = fs.readdirSync(fullPath)
      .filter((file) => this._supportedExt.indexOf(path.extname(file)) >= 0);

    for await (const file of files) {
      const modulePath = path.resolve(fullPath, file);
      try {
        let module = require(modulePath);
        module = module.default || module;
        module();
      } catch (err) {
        /* Since we might register folders that have API class defined in them
           and since they are ES6+ Classes, we cannot call them directly. */
        puzzle.log.error(`Unable to register the routes exposed by '${modulePath}'.`);
        puzzle.log.debug(err.message);
      }
    }
  }

  /**
   * Register an API routes folder into the engine.
   *
   * @param {string} root The root folder of the module.
   * @param {string} [routes=routes] The name of the routes folder.
   */
  async build(root, routes = "routes") {
    const fullPath = path.join(root, routes);
    const files = fs.readdirSync(fullPath)
      .filter((file) => this._supportedExt.indexOf(path.extname(file)) >= 0);

    for await (const file of files) {
      const modulePath = path.resolve(fullPath, file);
      try {
        let ClassModule = require(modulePath);
        ClassModule = ClassModule.default || ClassModule;
        if (!this.isValid(ClassModule) || (typeof ClassModule === "function" && !this.isValid(ClassModule.prototype))) {
          continue;
        }

        const classRoute = new ClassModule();
        if (!classRoute.noImport) {
          classRoute.build();
        }
      } catch (err) {
        /* Since we might register folders that have API class defined in them
           and since they are ES6+ Classes, we cannot call them directly. */
        puzzle.log.error(`Unable to register the routes exposed by class of '${modulePath}'.`);
        puzzle.log.debug(err.message);
        puzzle.log.debug(err.stack);
      }
    }
  }
}

module.exports = ControllerLoader;
