"use strict";

const puzzle = require("../Puzzle");

/**
 * URL Builder using the configured context path.
 *
 * @param {string} path The path to be correctly built.
 *
 * @return {string}
 */
function URLBuilder(path) {
  return `${puzzle.config.http.contextPath}/${path}`.replace(/[/]+/g, "/");
}

/**
 * Server URL Builder using the configured context path.
 *
 * @param {string} path The path to be correctly built.
 *
 * @return {string}
 */
function ServerURLBuilder(path) {
  return `${puzzle.config.http.serverURL}${URLBuilder(path)}`;
}


/**
 * Module Utils
 */
module.exports = {
  /**
   * URL Builder using the configured context path.
   *
   * @param {string} path The path to be correctly built.
   *
   * @return {string}
   */
  URLBuilder,
  /**
   * Server URL Builder using the configured context path.
   *
   * @param {string} path The path to be correctly built.
   *
   * @return {string}
   */
  ServerURLBuilder,
};
