"use strict";

/**
 * Application definition.
 *
 * @module Core
 *
 * @typedef {object} IApplication
 * @property {string} name The name of the application.
 * @property {string} version The version of the application.
 * @property {string[]} [modules] A list of application modules/puzzles.
 * @property {string[]} [puzzles] A list of application modules/puzzles.
 */
