"use strict";

const PObject = require("./PObject");

/**
 * Runtime class to be used in the framework.
 *
 * Using this class, you will be able to add functionalities in the various stages where
 * the framework will be at various points in its execution.
 *
 * This class is an abstract class. You have to extend it in order to use it.
 *
 * The lifecycle of the framework is: start -> BOOT -> SERVER_ONLINE(online) -> SHUTDOWN -> stop
 *
 * @module Core
 */
class PRuntime extends PObject {
  /**
   * Should this runtime be ran only in HTTP environment?
   *
   * @protected
   *
   * @property {boolean}
   */
  _httpOnly = false;

  /**
   * Should this runtime be ran only in HTTP environment?
   *
   * @return {boolean}
   */
  get httpOnly() {
    return this._httpOnly;
  }

  /**
   * Should this runtime be ran only in CLI environment?
   *
   * @protected
   *
   * @property {boolean}
   */
  _cliOnly = false;

  /**
   * Should this runtime be ran only in CLI environment?
   *
   * @return {boolean}
   */
  get cliOnly() {
    return this._cliOnly;
  }

  /**
   * Append functionality to the puzzle engine.
   *
   * @param {PEngine} engine The engine runtime.
   */
  use(engine) {

  }

  /**
   * The code that executes before the Boot status is achieved.
   */
  beforeBoot() {

  }

  /**
   * The code that executes when the Boot status is achieved.
   */
  boot() {

  }

  /**
   * The code that executes after the Boot status is achieved.
   */
  afterBoot() {

  }

  /**
   * The code that executes before the Server Online status is achieved.
   */
  beforeOnline() {

  }

  /**
   * The code that executes when the Server Online status is achieved.
   */
  online() {

  }

  /**
   * The code that executes after the Server Online status is achieved.
   */
  afterOnline() {

  }

  /**
   * The code that executes before the Shutdown status is achieved.
   */
  beforeShutdown() {

  }

  /**
   * The code that executes when the Shutdown status is achieved.
   */
  shutdown() {

  }

  /**
   * The code that executes after the Shutdown status is achieved.
   */
  afterShutdown() {

  }
}

module.exports = PRuntime;
