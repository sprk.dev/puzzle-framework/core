"use strict";

const PObject = require("./PObject");
const PUse = require("./PUse");
const PVersion = require("./PVersion");

/**
 * Engine base object.
 *
 * It defines some base elements and an extension point through the get/set methods. When you add
 * an extension point, the engine class defines a getter property for it to enable easy access to it.
 *
 * @module Core
 */
class PEngine extends PObject {
  /**
   * Holds the modules loaded directly on the engine.
   *
   * @protected
   *
   * @property {Map<string, PObject>}
   */
  _modules = {};

  /**
   * Holds a reference to the logger class.
   *
   * @protected
   *
   * @property {PLog|null}
   */
  _logger = null;

  /**
   * Constructor of the PEngine class. Initializes some base elements like: version, app, env etc.
   */
  constructor() {
    super();

    this.init();
  }

  /**
   * Holds information about the engine version.
   *
   * @protected
   *
   * @property {PVersion|null}
   */
  _version = null;

  /**
   * Getter for engine version.
   *
   * @return {PVersion}
   */
  get version() {
    return this._version;
  }

  /**
   * Setter for engine version.
   *
   * @param {PVersion|string} packageJson The frameworks package.json file.
   */
  set version(packageJson) {
    this._version = new PVersion(packageJson.version);
  }

  /**
   * Holds information about the application version.
   *
   * @protected
   *
   * @property {PVersion|null}
   */
  _appVersion = null;

  /**
   * Getter for application version.
   *
   * @return {PVersion}
   */
  get appVersion() {
    return this._appVersion;
  }

  /**
   * Holds information about the application.
   *
   * @protected
   *
   * @property {IApplication|null}
   */
  _app = null;

  /**
   * Getter for application information.
   *
   * @return {IApplication}
   */
  get app() {
    return this._app;
  }

  /**
   * Setter for app property. It also initializes the application version.
   *
   * @param {IApplication} packageJson The package.json of the application.
   */
  set app(packageJson) {
    this._app = {
      name: packageJson.name || this._app.name,
      version: packageJson.version || this._app.version,
      modules: packageJson.puzzles || packageJson.modules || []
    };

    this._appVersion = new PVersion(packageJson.version);
  }

  /**
   * Getter for application environment.
   *
   * @return {string}
   */
  get env() {
    return this.isValid(process.env.NODE_ENV) ? process.env.NODE_ENV : "development";
  }

  /**
   * Returns the logger instance.
   *
   * @return {PLog|null}
   */
  get log() {
    return this.isValid(this._logger)
      ? this._logger.logger || this._logger
      : null;
  }

  /**
   * Sets the logger instance.
   *
   * @param {PLog|null} logger The logger object.
   */
  set log(logger) {
    this._logger = logger;
  }

  /**
   * Returns the log level used by the logger.
   *
   * @return {string}
   */
  get logLevel() {
    return this.isValid(this._logger)
      ? this._logger.logLevel || ""
      : "";
  }

  /**
   * Module getter method.
   *
   * @param {string} module The name of the module.
   *
   * @return {PObject|null}
   */
  get(module) {
    return this.isValid(this._modules[module])
      ? this._modules[module]
      : undefined;
  }

  /**
   * Module setter method. Creates a getter function for the module set.
   *
   * @param {string} moduleName The name of the module to be added to the engine.
   * @param {PObject} moduleInstance The instance of the module to be added.
   */
  set(moduleName, moduleInstance) {
    this._modules[moduleName] = moduleInstance;
    Object.defineProperty(this, moduleName, {
      get: () => this.get(moduleName),
      configurable: true
    });
  }

  /**
   * Module unsetter method. Deletes the getter function for the module.
   *
   * @param {string} moduleName The name of the module to be added to the engine.
   */
  unset(moduleName) {
    if (!this.isValid(this._modules[moduleName])) {
      return;
    }
    delete this._modules[moduleName];
  }

  /**
   * Initializes the class.
   *
   * @protected
   */
  init() {
    this._version = new PVersion();
    this.app = {
      name: "Puzzle Framework",
      version: this._version.version,
      modules: []
    };
    this._logger = null;
  }

  /**
   * Use the given module. Attach some elements to the engine.
   *
   * @param {object|function} module The module to be used.
   */
  use(module) {
    if (!this.isValid(module) || Array.isArray(module)) {
      return;
    }
    if (typeof module === "function" && !this.isValid(module.prototype)) {
      module(this);
      return;
    }
    if (module !== null && typeof module === "object"
      && (module.prototype instanceof PUse || !!module.use)) {
      module.use(this);
    }
  }
}

module.exports = PEngine;
