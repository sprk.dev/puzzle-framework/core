"use strict";

const _ = require("lodash");
const { config } = require("dotenv");

const PObject = require("./PObject");

config();

/**
 * Configuration data store class.
 */
class PConfig extends PObject {
  /**
   * Configuration data store object
   *
   * @protected
   *
   * @property {Map<string, *>}
   */
  _config = {};

  /**
   * Which elements cannot be overwritten.
   *
   * @protected
   *
   * @property {string[]}
   */
  _unallowed = [
    "_config",
    "isEmpty",
    "isValid",
    "className",
    "init",
    "load",
    "clear",
    "get",
    "delete",
    "_reloadProperties"
  ];

  /**
   * Let's the user know if the datastore is empty.
   *
   * @return {boolean}
   */
  get isEmpty() {
    return this.isValid(this._config) && Object.keys(this._config).length === 0;
  }

  /**
   * Returns the keys of the configuration elements stored in the the datastore.
   *
   * @return {string[]}
   */
  get keys() {
    return Object.keys(this._config);
  }

  /**
   * Initializes the configuration.
   */
  init() {

  }

  /**
   * Loads some configuration elements into the datastore.
   *
   * @param {object} configElements The configuration elements that need to be loaded.
   */
  load(configElements) {
    this._config = _.merge(this._config, configElements);
    this.reloadProperties();
  }

  /**
   * Clears the data store.
   */
  clear() {
    Object.keys(this._config).forEach((key) => {
      delete this[key];
    });
    this._config = {};
  }

  /**
   * Returns the element at the given key.
   *
   * @param {string} key The key for which we need a value.
   *
   * @return {*}
   */
  get(key) {
    return this.isValid(this._config[key]) ? this._config[key] : undefined;
  }

  /**
   * Sets a value for a given key. Also, defines a getter/setter for the key.
   *
   * @param {string} key The key for which the value is set.
   * @param {*} value The value to be set.
   */
  set(key, value) {
    if (this._unallowed.includes(key)) {
      return;
    }
    this._config[key] = value;
    Object.defineProperty(this, key, {
      get: () => this.get(key),
      set: (newValue) => this.set(key, newValue),
      configurable: true
    });
  }

  /**
   * Deletes a key from the datastore.
   *
   * @param {string} key The key to be deleted.
   */
  delete(key) {
    if (this.isValid(this._config[key])) {
      delete this._config[key];
      delete this[key];
    }
  }

  /**
   * Returns a variable from the environment.
   *
   * @param {string} key The environment variable we need to read.
   *
   * @return {string}
   */
  env(key) {
    if (!this.isValid(key)) {
      return undefined;
    }

    return process.env[key];
  }

  /**
   * Reloads the properties defined in the datastore as getter/setters for the current object.
   */
  reloadProperties() {
    Object.keys(this._config).forEach((key) => {
      Object.defineProperty(this, key, {
        get: () => this.get(key),
        set: (newValue) => this.set(key, newValue),
        configurable: true
      });
    });
  }
}

module.exports = PConfig;
