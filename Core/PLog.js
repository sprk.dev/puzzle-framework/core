"use strict";

const PUse = require("./PUse");

/**
 * Logger class - enables the logging in the entire application.
 *
 * @module Core
 */
class PLog extends PUse {
  logLevel = "";

  logger = null;

  constructor() {
    super();
    this.initLog();
  }

  /**
   * Creates the logger object that is going to be used by the application.
   *
   * @param {string} [logLevel] The level of logging.
   */
  initLog(logLevel = "") {
  }

  /**
   * Attaches the logger to the engine.
   *
   * @param {PEngine} engine The reference to engine class
   */
  use(engine) {
    engine.log = this;
  }
}

module.exports = PLog;
