"use strict";

const PObject = require("./PObject");

/**
 * Class PUse
 */
class PUse extends PObject {
  /**
   * Called by engine when .use method is used.
   *
   * @param {PEngine} engine The reference to engine class.
   */
  use(engine) {

  }
}

module.exports = PUse;
