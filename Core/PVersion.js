"use strict";

const PObject = require("./PObject");

const VERSION = "2.0.0";

/**
 * Version information class.
 *
 * @module Core
 */
class PVersion extends PObject {
  /**
   * Major version number
   *
   * @private
   *
   * @property {number}
   */
  #major = 0;

  /**
   * Minor version number
   *
   * @private
   *
   * @property {number}
   */
  #minor = 0;

  /**
   * Patch version number
   *
   * @private
   *
   * @property {number}
   */
  #patch = 0;

  /**
   * Creates an instance of the version class.
   *
   * @param {string} version The version string (eg. 0.1.0).
   */
  constructor(version = "") {
    super();

    this.version = version || VERSION;
  }

  /**
   * Returns the version string.
   *
   * @return {string}
   */
  get version() {
    return `${this.#major}.${this.#minor}.${this.#patch}`;
  }

  /**
   * Sets the version of the application.
   *
   * @param {string} value The version string.
   */
  set version(value) {
    const versionString = value.split(".");

    this.#major = parseInt(versionString[0]);
    this.#minor = parseInt(versionString[1]);
    this.#patch = parseInt(versionString[2]);
  }
}

module.exports = PVersion;
