"use strict";

const AppState = require("./AppState");
const PRuntime = require("./PRuntime");

/**
 * State machine class to be used in the framework.
 *
 * @module Core
 */
class PState extends PRuntime {
  /**
   * Current state of the framework.
   *
   * @property {AppState}
   */
  _state = AppState.INVALID;

  /**
   * Returns the current state of the framework.
   *
   * @return AppState
   */
  get state() {
    return this._state;
  }

  /**
   * The code that executes when the Boot status is achieved.
   */
  boot() {
    super.boot();
    this._state = AppState.BOOT;
  }

  /**
   * The code that executes when the Server Online status is achieved.
   */
  online() {
    if (this._state !== AppState.BOOT) {
      this._state = AppState.INVALID;
      return;
    }
    super.online();
    this._state = AppState.ONLINE;
  }

  /**
   * The code that executes when the Shutdown status is achieved.
   */
  shutdown() {
    if (this._state !== AppState.BOOT && this._state !== AppState.ONLINE) {
      this._state = AppState.INVALID;
      return;
    }
    super.shutdown();
    this._state = AppState.SHUTDOWN;
  }

  /**
   * The code that executes after the Shutdown status is achieved.
   */
  afterShutdown() {
    if (this._state !== AppState.SHUTDOWN) {
      this._state = AppState.INVALID;
      return;
    }
    super.afterShutdown();
    this._state = AppState.HALT;
  }
}

module.exports = PState;
