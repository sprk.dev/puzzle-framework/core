"use strict";

/**
 * Application state
 *
 * @module Core
 *
 * @typedef {string} AppState
 */

module.exports = {
  BOOT: "boot",
  ONLINE: "online",
  SHUTDOWN: "shutdown",
  HALT: "halt",
  INVALID: ""
};
