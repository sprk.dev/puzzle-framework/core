"use strict";

/**
 * PObject - Base object of the Puzzle Framework
 *
 * @module Core
 */
class PObject {
  /**
   * Return the name of current class.
   */
  get className() {
    return this.constructor.name;
  }

  /**
   * Checks if the passed parameter is valid.
   *
   * @public
   *
   * @param {*} element The variable to be checked.
   */
  isValid(element) {
    return element !== null && element !== undefined;
  }
}

module.exports = PObject;
