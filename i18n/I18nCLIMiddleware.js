"use strict";

const CMiddleware = require("../CLI/CMiddleware");
const puzzle = require("../Puzzle");

/**
 * Class I18nHTTPMiddleware
 */
class I18nHTTPMiddleware extends CMiddleware {
  use(engine) {
    let locale = puzzle.config.i18n.defaultLocale;
    if (this.isValid(puzzle.config.env("PUZZLE_LANG"))) {
      locale = puzzle.config.env("PUZZLE_LANG");
    } else if (this.isValid(puzzle.config.env("PLANG"))) {
      locale = puzzle.config.env("PLANG");
    }

    engine.i18n.locale = locale;
  }
}

module.exports = I18nHTTPMiddleware;
