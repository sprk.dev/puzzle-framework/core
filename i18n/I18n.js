"use strict";

const format = require("string-format");
const fs = require("fs");
const path = require("path");

const PRuntime = require("../Core/PRuntime");

class I18n extends PRuntime {
  /**
   * The data used to translate messages.
   *
   * @private
   *
   * @property {Map<string, object>}
   */
  #data = {};

  /**
   * The current language used by the application.
   *
   * @private
   *
   * @property {string}
   */
  #locale = "en";

  /**
   * A list of supported languages that can be used in the application.
   *
   * @private
   *
   * @property {string[]}
   */
  #supportedLanguages = ["en"];

  /**
   * @return {string}
   */
  get locale() {
    return this.#locale;
  }

  /**
   * @param {string} newValue
   */
  set locale(newValue) {
    if (this.#supportedLanguages.indexOf(newValue) > 0) {
      this.#locale = newValue;
    }
  }

  /**
   * @param {PEngine} engine
   */
  use(engine) {
    engine.set("i18n", this);
    this.init(engine);
  }

  /**
   * Performs some initializations.
   *
   * @param {PEngine} engine
   */
  init(engine) {
    this.#supportedLanguages = engine.config.i18n.languages;
    this.#data = {};
    this.#supportedLanguages.forEach((language) => {
      this.#data[language] = JSON.parse(fs.readFileSync(path.join(process.cwd(), engine.config.i18n.locales, `${language}.json`), "utf-8"));
    });

    this.#locale = engine.config.i18n.defaultLocale;
  }


  /**
   * Translation method. Takes the message label key and a bunch of parameters and
   * builds the final message with them.
   *
   * @param {Error|string|object} labelKey The Label Key.
   * @param {...*} params The Parameters to be passed to the message.
   *
   * @return {string}
   */
  __(labelKey, ...params) {
    return format(this._extractLabelKey(labelKey), ...params);
  }

  /**
   * Translation method. Takes the message label key and a bunch of parameters and
   * builds the final message with them.
   *
   * @param {Error|string|object} labelKey The Label Key.
   * @param {...*} params The Parameters to be passed to the message.
   *
   * @return {string}
   */
  __n(labelKey, ...params) {
    return this.__(labelKey, ...params);
  }

  /**
   * Method used to extract the correct label, in order to
   * translate a message on the screen.
   *
   * @param {Error|string|object} labelKey The key to be converted.
   *
   * @return {string}
   */
  _extractLabelKey(labelKey) {
    if (labelKey instanceof Error) {
      return labelKey.message;
    }

    if (typeof labelKey !== "string") {
      return labelKey.constructor.name;
    }

    let locale = this.#data[this.#locale];
    if (labelKey.indexOf(".") < 0) {
      return locale[labelKey] || labelKey;
    }

    if (this.isValid(locale[labelKey])) {
      return locale[labelKey];
    }

    const labelTree = labelKey.split(".");
    for (const treeElement of labelTree) {
      if (!this.isValid(locale[treeElement])) {
        return labelKey;
      }

      locale = locale[treeElement];
    }
    return locale || labelKey;
  }
}

module.exports = I18n;
