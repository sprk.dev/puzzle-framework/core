"use strict";

const PMiddleware = require("../HTTP/PMiddleware");

/**
 * Class I18nHTTPMiddleware
 */
class I18nHTTPMiddleware extends PMiddleware {
  use(engine, http) {
    http.use((req, res, next) => {
      engine.i18n.locale = this.isValid(req.cookies)
        ? req.cookies[engine.config.i18n.cookie]
        : engine.config.i18n.defaultLocale;

      /**
       * Returns a translated message to be used in various situations.
       *
       * @memberOf {http.ServerResponse}
       *
       * @param {string} labelKey The Label Key.
       * @param {...*} params The Parameters to be passed to the message.
       *
       * @return {string}
       */
      res.__ = (labelKey, ...params) => engine.i18n.__(labelKey, ...params);

      /**
       * Returns an error to the API.
       *
       * @memberOf {http.ServerResponse}
       *
       * @param {string} labelKey The Label Key.
       * @param {...*} params The Parameters to be passed to the message.
       *
       * @return {http.ServerResponse}
       */
      res.__ok = (labelKey, ...params) => res.ok(engine.i18n.__(labelKey, ...params));
      next();
    });
  }
}

module.exports = I18nHTTPMiddleware;
