"use strict";

const PError = require("./PError");
const HTTPStatus = require("../HTTP/HTTPStatus");

/**
 * Class HTTPError
 *
 * @module Exception
 */
class HTTPError extends PError {
  httpStatus = HTTPStatus.SERVER_ERROR;
}

module.exports = HTTPError;
