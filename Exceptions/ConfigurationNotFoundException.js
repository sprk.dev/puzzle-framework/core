"use strict";

const PError = require("./PError");

/**
 * Class ConfigurationNotFoundException
 */
class ConfigurationNotFoundException extends PError {
  constructor(...configParams) {
    super(`The desired configuration parameter(s): [${configParams.join(", ")}] cannot be found!`);
  }
}

module.exports = ConfigurationNotFoundException;
