"use strict";

const PError = require("./PError");

/**
 * Class InvalidInstanceTypeException
 */
class InvalidInstanceTypeException extends PError {
  constructor(desiredClass) {
    super(`The passed instance doesn't have the desired (${desiredClass}) type!`);
  }
}

module.exports = InvalidInstanceTypeException;
