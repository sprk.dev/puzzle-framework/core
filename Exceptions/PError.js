"use strict";

class PError extends Error {
  extra = "";
}

module.exports = PError;
