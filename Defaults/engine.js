"use strict";

/**
 * General engine configuration
 */
module.exports = {
  /**
   * Is the application in debug mode? Overrides the log setting.
   *
   * @type {boolean}
   */
  debug: false,

  /**
   * Log configuration
   *
   * @type {Object}
   */
  log: {
    /**
     * Log level: emergency, alert, critical, error, warning, notice, info, debug
     *
     * @type {string}
     */
    level: "info",

    /**
     * Location of the log file.
     *
     * @type {string}
     */
    file: "",

    /**
     * Should the log be rotated?
     *
     * @type {boolean}
     */
    rotate: true,

    /**
     * Maximum log file size before rotation. Modifiers are: k(ilo), m(ega), g(iga)
     *
     * @type {string}
     */
    size: "50k",

    /**
     * How often to check for file rotation conditions. possible values are '1s', '1m', '1h'. default is 5m.
     *
     * @type {string}
     */
    schedule: "5m",

    /**
     * Should the rotated log be compressed?
     *
     * @type {boolean}
     */
    compress: false,

    /**
     * How many rotation files should exist.
     *
     * @type {int}
     */
    count: 3
  },

  /**
   * Configuration of the task runner.
   *
   * @type {Object}
   */
  runner: {
    /**
     * Log level: emergency, alert, critical, error, warning, notice, info, debug
     *
     * @type {string}
     */
    level: "info",

    /**
     * The location of the task runner logging folder.
     *
     * @type {string}
     */
    logs: "logs",

    /**
     * Timestamp used to identify the runner log file.
     *
     * @type {string}
     */
    timestamp: "YYYYMMDD",
  },

  /**
   * Name of the application.
   *
   * @type {string}
   */
  name: "Puzzle Framework"
};
