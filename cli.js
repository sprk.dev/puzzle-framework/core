"use strict";

const puzzle = require("./Puzzle");
const ConsoleLogger = require("./Logger/ConsoleLogger");
const CLI = require("./CLI/CLI");
const ConsoleBootstrap = require("./Bootstrap/ConsoleBootstrap");

let code = 0;
puzzle.use(new ConsoleLogger());
puzzle.modules.register("Bootstrap", new ConsoleBootstrap());
puzzle.modules.register("CLI", new CLI());
CLI.cli.finishCommand = async () => {
  await puzzle.modules.shutdown();
  process.exit(code);
};

puzzle.use((engine) => {
  engine.set("boot", async () => {
    try {
      await engine.modules.loadFromPackage();
      await engine.modules.boot();
      await engine.modules.online();
      await CLI.run();
    } catch (e) {
      /* eslint-disable no-console */
      console.error(e.message);
      if (engine.config.engine.debug) {
        console.dir(e);
      }
      /* eslint-enable no-console */
      code = e.code || 1;
    }
  });
});

module.exports = puzzle;
