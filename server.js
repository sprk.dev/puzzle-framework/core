"use strict";

const puzzle = require("./Puzzle");
const ServerLogger = require("./Logger/ServerLogger");
const HTTP = require("./HTTP/HTTP");
const ControllerLoader = require("./HTTP/Controller/ControllerLoader");
const ServerBootstrap = require("./Bootstrap/ServerBootstrap");

puzzle.use(new ServerLogger());
puzzle.modules.register("Bootstrap", new ServerBootstrap());
puzzle.modules.register("HTTP", new HTTP());
puzzle.use(new ControllerLoader());
puzzle.use((engine) => {
  engine.set("boot", async () => {
    await engine.modules.loadFromPackage();
    await engine.modules.boot();
    await engine.modules.online();
  });
  engine.set("shutdown", async () => {
    await engine.modules.shutdown();
  });
});

module.exports = puzzle;
