"use strict";

module.exports = {
  cli: require("./cli"),
  server: require("./server")
};
